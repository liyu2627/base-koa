/**
 * 项目公共配置
 */
module.exports = {
    localPort: 8181, //本地端口
    remotePort: 9181 //远程端口
};
