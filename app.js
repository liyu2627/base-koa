/**
 * 项目入口文件
 */
const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const views = require('koa-views');
const static = require('koa-static');
const path = require('path');

const config = require('./app/config/index');
//创建Koa对象
const app = new Koa();
const router = new Router();

//配置ctx.body解析中间件
app.use(bodyParser());

//配置静态资源加载中间件
app.use(static(path.join(__dirname, './app/views')));

// 配置模板引擎中间件
app.use(
    views(path.join(__dirname, './app/views'), {
        map: { html: 'ejs' }
    })
);

//配置路由中间件
router.get('/', async (ctx) => {
    await ctx.render('index', { title: '我是基础项目结构' });
});
app.use(router.routes()).use(router.allowedMethods());

//监听端口
app.listen(config.localPort);
app.listen(config.remotePort);
console.log(`http://localhost:${config.localPort}/ (本地数据版)`);
console.log(`http://localhost:${config.remotePort}/ (后端数据版)`);
